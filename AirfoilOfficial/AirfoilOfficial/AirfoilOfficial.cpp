// AirfoilOfficial.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<cmath>
#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<algorithm>
#include <iomanip> 

using namespace std;

#define NUMBEROFPOINTS 100
#define SPEED 100
#define ATTACK_ANGLE 30
#define AIRWEIGHT 1.225
#define POPSIZE 100
#define CROSSING_COEFFICIENT 0.5
#define MUTATION_COEFFICIENT 0.1
#define BREAKPOINT 70



struct Airfoil {
	double points[NUMBEROFPOINTS];
	float liftForce;
	float surface;
	double deflectionAngle[NUMBEROFPOINTS];
};

bool operator<(Airfoil &father, Airfoil &mother) {
	return father.liftForce > mother.liftForce;
}

void crossing(Airfoil &father, Airfoil &mother);
void mutation(Airfoil &father);
void getLiftForce(Airfoil &father);
void createAirfoils(Airfoil airfoils[]);

int _tmain(int argc, _TCHAR* argv[]) {
	srand ((unsigned int)time(NULL));
	float breakStop = 10 * 1000;

	Airfoil population[POPSIZE];
	int generation = 0;

	createAirfoils(population);

	sort(population, population+POPSIZE);
    

    while(generation < 1000) {
        sort(population, population+POPSIZE);

		for(int i = 0; i < POPSIZE*CROSSING_COEFFICIENT; i++) {
           crossing(population[i], population[i+2]);   
    	}

		for(int i = 0; i < POPSIZE*MUTATION_COEFFICIENT; i++) {
			mutation(population[i]);	
        }

		if (population[0].liftForce > breakStop){
            break;
        }

		generation++;
    }
    
	char table[NUMBEROFPOINTS][NUMBEROFPOINTS];
	float tableOfValues[NUMBEROFPOINTS][NUMBEROFPOINTS];
	for(int i=0;i<NUMBEROFPOINTS;i++) {
		for(int j=0;j<NUMBEROFPOINTS;j++) {
			table[i][j]='-';
		}
	}

	int ff = 0;
	int pops = 0;
	for(int i=0;i < NUMBEROFPOINTS;i++) {

		if(i == 0) {
			table[ff][i] = '*';
			ff++;
		} else {
			float before = static_cast<float>( static_cast<int>(population[0].points[pops-1]*100) ) / 100;
			float now = static_cast<float>( static_cast<int>(population[0].points[pops]*100) ) / 100;
			if(now > before) {
				table[ff][i] = '*';
				ff++;
			} else if(now < before) {
				table[ff-1][i] = '*';
				ff = ff-1;

			} else if(now == before) {
				table[ff-1][i] = '*';		
			}
		}
		pops++;
	}

	cout<<endl;

		for(int i=0;i<NUMBEROFPOINTS;i++) {
			for(int j=0;j<NUMBEROFPOINTS;j++) {
				cout<<table[i][j];
			}
				cout<<endl;
		}

	cout<<endl;



	for(int i = 0; i < NUMBEROFPOINTS; i++){
      //cout<<setprecision(3)<<population[0].points[i] << "\n";
    }

	cout<<endl<<endl<<"LIFT FORCE: "<<population[0].liftForce<<endl<<endl;
	cout<<endl<<endl<<"Generation: "<<generation<<endl<<endl;
	system("pause");	
	return 0;
}


void crossing(Airfoil &father, Airfoil &mother) {
	for(int i = 0; i < NUMBEROFPOINTS*CROSSING_COEFFICIENT; i++) {
			int tempIndex = 0;
			tempIndex = father.points[i];
			father.points[i] = mother.points[i];
			mother.points[i] = tempIndex;
	}

	getLiftForce(father);
	getLiftForce(mother);
}

void createAirfoils(Airfoil airfoils[]) {
	float holder = 0.0;
    float randomize;

	for(int i = 0; i < POPSIZE; i++) {

		for(int j = 0; j < BREAKPOINT; j++) {
			randomize = (float) (rand() % 500) / 100.0 + holder;
			holder = randomize;
			airfoils[i].points[j] = holder/1000;
		}

		for(int j = BREAKPOINT; j < NUMBEROFPOINTS; j++) {
			randomize = holder - (float) (rand() % 500) / 100.0;
			if (randomize < 0){
				randomize = 0;
			}
			holder = randomize;
			airfoils[i].points[j] = holder/1000;
		}

		airfoils[i].liftForce = 0;
		airfoils[i].surface = 0;
		getLiftForce(airfoils[i]);
	}
}

void mutation(Airfoil &father) {
	float holder = 0.0;
	float randomize;
	for(int j = 0; j < NUMBEROFPOINTS-50; j++) {
		randomize = (float) (rand() % 500) / 100.0 + holder;
		holder = randomize;
		father.points[j] = holder/1000;
	}

	father.liftForce = 0;
	father.surface = 0;
	getLiftForce(father);
}

void getLiftForce(Airfoil &father) {
	float vectorLength;
	float coefOfAngle;
	for (int j=0; j < BREAKPOINT; j++){
        vectorLength = sqrt( (j * j) + (father.points[j] * father.points[j]) );
        coefOfAngle = asin(father.points[j]/vectorLength);
        
		father.deflectionAngle[j] = coefOfAngle + ATTACK_ANGLE;
    }
    
    for (int j=BREAKPOINT; j < NUMBEROFPOINTS; j++){
        vectorLength = sqrt( (j * j) + (father.points[j] * father.points[j]) );
        coefOfAngle = asin(father.points[j]/vectorLength);
        
        father.deflectionAngle[j] = coefOfAngle - ATTACK_ANGLE;
    }
    
	father.surface = 0;

	for (int i=0; i < NUMBEROFPOINTS; i++){
		father.surface += father.points[i];
	}
    
    float totalDeflection = 0.0;
	for(int i = 0; i < NUMBEROFPOINTS; i++){
        totalDeflection += father.deflectionAngle[i];
    }
    totalDeflection /= 100;
    
    float coefficient = ATTACK_ANGLE / totalDeflection;
    
	father.liftForce = ((coefficient * AIRWEIGHT * father.surface * SPEED * SPEED/2));
	
    if(father.liftForce < 0){
        father.liftForce = father.liftForce * (-1);
    }

}
